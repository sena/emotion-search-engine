# Emotion Search Engine

Emotion Search Engine is a Web-based app for Search image on your folder or image File based on Emotion.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install app.

```bash
pip install Flask
pip install Werkzeug
pip install glob2
pip install os-sys
pip install opencv-python
pip install numpy
pip install tensorflow
pip install pickle5
pip install PySimpleGUI
```

## Use Case

![Use Case](/imageReadme/3.jpg)

## Dev Configuration

![Use Case](/imageReadme/Dev Conf.jpg)
![Use Case](/imageReadme/1.jpg)

## User Interface
Search Emotion Engine
![GitHub Logo](/imageReadme/8.png)

Face Emotion Recognition
![GitHub Logo](/imageReadme/10.jpeg)

Mobile Responsive 
![GitHub Logo](/imageReadme/9.png)
from app import app
from flask import  flash, request, redirect, render_template
from werkzeug.utils import secure_filename
import glob, os, os.path
import cv2
import numpy as np
from tensorflow.keras.models import model_from_json
from tensorflow.keras.preprocessing import image
import pickle
import PySimpleGUI as sg


#load model
model = model_from_json(open("model/model.json", "r").read())
#load weights
model.load_weights('model/check_model.h5')
#load face
face_haar_cascade = cv2.CascadeClassifier('src/cascades/data/haarcascade_frontalface_alt2.xml')

class Foto:
    def cekfoto(path, img, gray_img, objectFaces):
        for (x, y, w, h) in objectFaces:
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), thickness=7)
            # cropping region of face area (Face of Interest)
            grayFoi = gray_img[y:y + w, x:x + h]
            grayFoi = cv2.resize(grayFoi, (48, 48))
            imgToAray = image.img_to_array(grayFoi)
            imgToAray = np.expand_dims(imgToAray, axis=0)
            imgToAray /= 255
            emoPredictions = model.predict(imgToAray)
            topListIndex = np.argmax(emoPredictions[0])
            emotionsList = ('angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral')
            predictedEmotion = emotionsList[topListIndex]
            return predictedEmotion

    def cekRecognizer(self):
        sg.theme("Dark Green 5")

        layout = [
            [sg.Text("Welcome to Emotion Recognition", size=(60, 1))],
            [sg.Image(filename="", key="-IMAGE-")],
            [sg.Button("Exit", size=(10, 1))],
        ]

        window = sg.Window("Live Emotion Recognition ",
                           layout,
                           location=(10, 10))

        cap = cv2.VideoCapture(0)

        while True:
            ret, test_img = cap.read()
            if not ret:
                continue

            imgGray = cv2.cvtColor(test_img, cv2.COLOR_BGR2GRAY)
            findFaces = face_haar_cascade.detectMultiScale(imgGray, 1.32, 5)

            for (x, y, w, h) in findFaces:
                faceGray = imgGray[y:y + w, x:x + h]
                faceGray = cv2.resize(faceGray, (48, 48))
                imgArray = image.img_to_array(faceGray)
                imgArray = np.expand_dims(imgArray, axis=0)
                imgArray /= 255
                emoPredict = model.predict(imgArray)
                indxArray = np.argmax(emoPredict[0])
                emotions = ('angry',
                            'disgust',
                            'fear',
                            'happy',
                            'sad',
                            'surprise',
                            'neutral')
                preEmotion = emotions[indxArray]
                if preEmotion == 'happy':
                    preEmotion = "Hi, Keep your smile"
                if preEmotion == 'sad':
                    preEmotion = "Hi, Don't be sad"
                if preEmotion != 'sad' and preEmotion == 'happy':
                    preEmotion = ("your emotion is" + preEmotion + "")
                print(preEmotion)
                cv2.putText(test_img,
                            preEmotion,
                            (10, (int(y) + 200)), cv2.FONT_HERSHEY_SIMPLEX, 1, (50, 20, 255) , 2)

            close, values = window.read(timeout=20)
            if close == "Exit" or close == sg.WIN_CLOSED:
                break

            # resized_img = cv2.resize(test_img, (1000, 700))
            test_img = cv2.resize(test_img, (1200, 700))
            imgbytes = cv2.imencode(".png", test_img)[1].tobytes()
            window["-IMAGE-"].update(data=imgbytes)
            # window.Maximize()
        window.close()

class SearchEngine:
    def __init__(self):
        self.file_index = []  # directory listing returned by os.walk()
        self.results = []  # search results returned from search method
        self.matches = 0  # count of records matched
        self.records = 0  # count of records searched

    def indexList(self, root_path):
        self.file_index = [(root, files) for root, dirs, files in os.walk(root_path) if files]
        # save index to file
        with open('index.pkl', 'wb') as f:
            pickle.dump(self.file_index, f)

    def search(self, term, search_type='contains'):
        self.results.clear()
        self.matches = 0
        self.records = 0

        for path, files in self.file_index:
            for file in files:
                self.records += 1
                if (search_type == "contains" and term.lower() in file.lower()):
                    result = path.replace('\\', '/') + '/' + file
                    self.results.append(result)
                    self.matches += 1
                else:
                    continue


        with open('results.txt', 'w') as f:
            for row in self.results:
                f.write(row + '\n')


pictExtensions = set(['png', 'jpg', 'jpeg', 'jfif', 'gif'])


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in pictExtensions

@app.route('/')
def upload_form():
    return render_template('index.html')

@app.route('/liveRecognizer')
def background_process_test():
    Foto.cekRecognizer(self="")

@app.route('/', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the files part
        if 'files[]' not in request.files:
            flash('No file part')
            return redirect(request.url)
        emotion_answer = ""

        if 'submit' in request.form:
            emotion_answer = request.form['emotion']
            if emotion_answer == 'all':
                emotion_answer = ""

        #delete all file
        filelist = glob.glob(os.path.join("static/uploaded", "*"))
        for f in filelist:
            os.remove(f)

        #upload file
        files = request.files.getlist('files[]')
        for file in files:
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

        s = SearchEngine()
        s.indexList("static/uploaded")
        s.search('')

        #look from the folder
        for path in s.results:
            img = cv2.imread(path)
            imgToGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            objectFaces = face_haar_cascade.detectMultiScale(imgToGray, 1.3, 5)
            predicted_emotion = Foto.cekfoto(path, img, imgToGray, objectFaces)
            hel = str(predicted_emotion)
            print(hel)
            if hel not in path:
                dirFolder = os.path.dirname(path)
                dirFile = os.path.basename(path)
                os.rename(path, dirFolder + "/" + hel + dirFile)

        #show the pict
        b = SearchEngine()
        b.indexList("static/uploaded")
        b.search(emotion_answer)
        for matchs in b.results:
            flash("../"+matchs)
            print(matchs)

        return redirect('/')

if __name__ == "__main__":
    app.run(threaded=False)